#!/bin/sh

# update ubuntu 20.04
sudo apt update && sudo apt -y upgrade;

# install essential curl file git zsh
sudo apt -y install build-essential curl file git;

# install Homebrew: (https://brew.sh/)
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)";

test -d /home/linuxbrew/.linuxbrew && echo 'eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)"' >> ~/.bash_profile;
test -d /home/linuxbrew/.linuxbrew && eval "$(/home/linuxbrew/.linuxbrew/bin/brew shellenv)";

# homebrew dependecies
brew install gcc && \
brew install wget curl git git-lfs && \
brew install pkg-config libsodium msgpack dos2unix && \
brew install imagemagick && \
brew install ffmpeg 

git config --global pager.branch false && \
git lfs install

# Install Mysql 5.7:
brew install mysql@5.7 && \
echo 'export PATH="/home/linuxbrew/.linuxbrew/opt/mysql@5.7/bin:$PATH"' >> ~/.bash_profile;
source ~/.bash_profile;

mysql.server start;

# Install Php 7.4.x:
brew install php@7.4 && \
echo 'export PATH="/home/linuxbrew/.linuxbrew/opt/php@7.4/bin:$PATH"' >> ~/.bash_profile && \
echo 'export PATH="/home/linuxbrew/.linuxbrew/opt/php@7.4/sbin:$PATH"' >> ~/.bash_profile;
source ~/.bash_profile;

# Php extensions
cd ~/ ; 
brew install composer;
rm $(brew --prefix php@7.4)/pecl && mkdir $(brew --prefix php@7.4)/pecl;

cd $(brew --prefix php@7.4)/pecl && \
ln -s ../lib/php/$(basename $(php-config --extension-dir)) . && \
sed -i 's/extension_dir/;extension_dir/g' $(brew --prefix)/etc/php/7.4/php.ini && \
cd ~/ && rm -rf /tmp/pear ~/.pearrc

pear update-channels;
pecl update-channels;

export CPPFLAGS=$(pkg-config --cflags libpcre2-32);
pecl install igbinary;
pecl install msgpack;
pecl install mongodb;
pecl install redis;
pecl install imagick;

# Install NVM 
# https://github.com/nvm-sh/nvm#installation-and-update
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.0/install.sh | bash;
export NVM_DIR="$HOME/.nvm"; 
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"; 
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion";

# Install de Nodejs, npm, yarn, y sass:
nvm install --lts;
npm install --global yarn;
brew install sass/sass/sass;

#
echo -e "if [ -f ~/.bash_profile ]; then\n\t. ~/.bash_profile\nfi" >>  ~/.bashrc
echo -e "\necho -e \"#######################################\\\nStarting BV.studio environment\\\n#######################################\\\n\\\n\"" >>  ~/.bashrc